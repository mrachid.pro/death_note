//
//  NotifServices.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 27/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import Foundation
import UserNotifications


class NotifService {
    static let shared = NotifService()

    private init() {}

    func notif(lastname: String, firstname: String, date: Date) {

        let content = UNMutableNotificationContent()
        content.title = "New death"
        content.subtitle = "\(lastname) \(firstname) is dead"

        let date = Date(timeIntervalSinceReferenceDate: date.timeIntervalSinceReferenceDate)
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)

        let identifier = "deathNotif"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            if let error = error {
                
            }
        })

    }
    func notifAccess() {
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
    }
    
}
