//
//  DateHelper.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 26/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import Foundation

class DateHelper {

    /**
     Format date 24/01/2017, 10:00AM
     */
    static let shortDateWithHourFormater: DateFormatter = {
        let dateFormater = DateFormatter()
        dateFormater.setLocalizedDateFormatFromTemplate("dd/MM/yyyy HH:mm")

        return dateFormater
    }()
}
