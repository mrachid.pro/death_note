//
//  human.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 25/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import UIKit
import RealmSwift

class Human: Object {

    /// the human lastname
    @objc dynamic var lastname: String = ""

    /// the human fristname
    @objc dynamic var firstname: String = ""

    /// the description of the death of the human
    @objc dynamic var describ: String = ""

    /// The date of death of the human
    @objc dynamic var date: Date = Date()

    /// The human image
    @objc dynamic var image: String = ""

    ///Initilisation data human
   convenience init(lastname: String, firstname: String, describ: String, date: Date, image: String) {
        self.init()
        self.lastname = lastname
        self.firstname = firstname
        self.describ = describ
        self.date = date
        self.image = image
    }
}
