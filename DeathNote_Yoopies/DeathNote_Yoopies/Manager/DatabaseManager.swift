//
//  DatabaseManager.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 26/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import RealmSwift
import Realm

final class DatabaseManager {

    static let shared = DatabaseManager()

    typealias HumansSuccess = (_ humans: Results<Human>) -> ()
    typealias AddHumansSuccess = (_ newDeath: Bool) -> ()

    var realm : Realm

    private init() {
        self.realm = try! Realm()
    }

    /**
     Get all dead human from database

     - parameter success: closure success
     */
    func getAllHumansDeath(success: @escaping HumansSuccess) {
        success(self.realm.objects(Human.self))
    }

    /**
     Add a new death in DB

     - parameter death: Human data to insert in DB
     - parameter success: closure success
     */
    func addNewDeath(death: Human, success: @escaping AddHumansSuccess) {
        try! realm.write {
            realm.add(death)
        }
        success(true)
    }
}










