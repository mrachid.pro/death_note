//
//  FilesManager.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 26/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import Foundation
import UIKit

final class FilesManager {

    static let shared = FilesManager()
    private let fileManager = FileManager.default

    private init() {}

    /**
     Save human image into directory

     - parameter image: Human image
     - parameter imageName: Human imageName
     */
    func saveHumanImage(image: UIImage, imageName: String){
        var paths = getDirectoryPath()
        paths.append(imageName)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths, contents: imageData, attributes: nil)
    }

    /**
     Get human image from directory

     - parameter name: Human imageName
     */
    func getHumanImage(name: String) -> UIImage? {
        var paths = getDirectoryPath()
        paths.append(name)
        if fileManager.fileExists(atPath: paths){
            return UIImage(contentsOfFile: paths)
        }
        return nil
    }

    /**
     Get directory path

     - returns: directory path
     */
    private func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        var documentsDirectory = paths[0]
        documentsDirectory.append("/DeathImages/")
        return documentsDirectory
    }

    /**
     Create directory folder for save image death human
     */
    func createDirectory() {
        var paths = getDirectoryPath()
        if !fileManager.fileExists(atPath: paths){
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        } else {
            print("Already dictionary created.")
        }
    }
}
