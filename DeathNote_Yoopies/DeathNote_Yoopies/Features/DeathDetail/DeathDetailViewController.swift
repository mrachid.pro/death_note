//
//  DeathDetailViewController.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 25/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import UIKit

class DeathDetailViewController: UIViewController {

    //MARK: - IBOUTLET
    @IBOutlet weak var deathUserImageView: UIImageView!
    @IBOutlet weak var deathUsernameLabel: UILabel!
    @IBOutlet weak var deathUserDate: UILabel!
    @IBOutlet weak var deathDescribTextView: UITextView!

    //MARK: - VARIABLE
    var deathInfo: Human? 

    //MARK: - LYFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInformation()
    }

    //MARK: - UTILS

    /**
     Setup death human information
    */
    private func setupInformation() {
        guard let death = deathInfo else {
            return
        }

        deathUsernameLabel.text = "\(death.firstname) \(death.lastname)"
        deathDescribTextView.text = death.describ

        let dateString = DateHelper.shortDateWithHourFormater.string(from: death.date)
        deathUserDate.text = dateString

        let image = FilesManager.shared.getHumanImage(name: "\(death.firstname)_\(death.lastname)_\(death.date.timeIntervalSinceReferenceDate)")
        deathUserImageView.image = image

    }

}
