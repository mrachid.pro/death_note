//
//  NewDeathViewController.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 25/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import UIKit

protocol DeathNoteActionDelegate: class {
    func addNewDeath()
}

class NewDeathViewController: UIViewController {

    //MARK: - IBOUTLET
    @IBOutlet weak var deathDateDatePicker: UIDatePicker!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var describTextView: UITextView!

    //MARK: - VARIABLE
    weak var delegate: DeathNoteActionDelegate?
    var imagePicker = UIImagePickerController()


    //MARK: - LYFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConfig()
    }

    //MARK: - ACTION
    @IBAction func cancelDeathAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func newDeathAction(_ sender: Any) {
        guard let human = createHuman() else {
            return
        }
        createDeath(death: human)
    }

    //MARK: - UTILS

    /**
     Setup configuration
     */
    private func setupConfig() {
        NotifService.shared.notifAccess()
        let tap = UITapGestureRecognizer(target: self, action: #selector(takePicture))
        userImageView.addGestureRecognizer(tap)
        userImageView.isUserInteractionEnabled = true
        deathDateDatePicker.minimumDate = Date()
        firstnameTextField.delegate = self
        lastnameTextField.delegate = self
        describTextView.delegate = self
    }

    /**
     Create dead human save it in DB and reload Tableview with delegate

     parameter death: Human data
     */
    private func createDeath(death: Human) {
        DatabaseManager.shared.addNewDeath(death: death) { value in
            if value {
                NotifService.shared.notif(lastname: self.lastnameTextField.text ?? "", firstname: self.firstnameTextField.text ?? "", date: self.deathDateDatePicker.date)
                if let delegate = self.delegate {
                    delegate.addNewDeath()
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                print("Error")
            }
        }
    }

    /**
     Create human data and save image into directory

     return: Human data
     */
    private func createHuman() -> Human? {

        guard let firstname = firstnameTextField.text?.checkValidtyUserInfo(), let lastname = lastnameTextField.text?.checkValidtyUserInfo(), let describ = describTextView.text.checkValidtyUserInfo(), let image = userImageView.image else {
            missingInformation()
            return nil
        }

        let datetime = deathDateDatePicker.date.timeIntervalSinceReferenceDate
        let imageName = "\(firstname)_\(lastname)_\(datetime)"
        let human = Human(lastname: lastname, firstname: firstname, describ: describ, date: deathDateDatePicker.date, image: imageName)
        FilesManager.shared.saveHumanImage(image: image, imageName: imageName)
        
        return human
    }

    /**
     Alert missing information
    */
    private func missingInformation() {
        let alert = UIAlertController(title: "Kira", message: "You miss some information", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(OKAction)
        present(alert, animated: true, completion: nil)
    }
}

//MARK: - IMAGE PICKER DELEGATE

extension NewDeathViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @objc private func takePicture(sender: UITapGestureRecognizer? = nil) {
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImageView.image = image
        } else {
            print("Something went wrong")
        }
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - TEXT DELEGATE

extension NewDeathViewController: UITextFieldDelegate, UITextViewDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension String {
    func checkValidtyUserInfo() -> String? {
        if self.isEmpty {
            return nil
        }
        return self
    }
}



