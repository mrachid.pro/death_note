//
//  DeathNoteListViewController.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 25/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import UIKit
import RealmSwift

class DeathNoteListViewController: UIViewController {

    //MARK: - IBOUTLET
    @IBOutlet weak var deathTableView: UITableView!

    //MARK: - VARIABLE
    var deathList : Results<Human>?

    //MARK: - LYFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem()
        registerCell()
        getHumansList()
    }

    //MARK: - UTILS

    /**
     Get all human dead
     */
    private func getHumansList() {
        DatabaseManager.shared.getAllHumansDeath { result in
            self.deathList = result
            self.deathTableView.reloadData()
        }
    }

    private func registerCell() {
        deathTableView.register(UINib(nibName: "DeathNoteTableViewCell" , bundle: nil), forCellReuseIdentifier: "deathNoteCell")
    }

    //MARK: - SEGUE

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let ndvc =  segue.destination as? NewDeathViewController else { return }
        ndvc.delegate = self
    }
}

//MARK: - DEATH DELEGATE
extension DeathNoteListViewController: DeathNoteActionDelegate {
    func addNewDeath() {
        deathTableView.reloadData()
    }
}

//MARK: - TABLEVIEW
extension DeathNoteListViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let death = deathList {
            return death.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "deathNoteCell", for: indexPath) as? DeathNoteTableViewCell  else {
            return UITableViewCell()
        }

        let death = deathList![indexPath.row]

        cell.setupCell(death: death)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let ddvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeathDetailViewController") as? DeathDetailViewController else { return }
        ddvc.deathInfo = deathList![indexPath.row]
        self.navigationController!.pushViewController(ddvc, animated: true)
    }
}
