//
//  DeathNoteTableViewCell.swift
//  DeathNote_Yoopies
//
//  Created by Mahmoud RACHID on 25/04/18.
//  Copyright © 2018 Mahmoud RACHID. All rights reserved.
//

import UIKit

class DeathNoteTableViewCell: UITableViewCell {

    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    /**
     Setup cell with data human

     parameter death: Human data
     */
    func setupCell(death: Human) {

        firstnameLabel.text = death.firstname
        lastnameLabel.text = death.lastname
        let dateString = DateHelper.shortDateWithHourFormater.string(from: death.date)
        dateLabel.text = dateString
        let image = FilesManager.shared.getHumanImage(name: "\(death.firstname)_\(death.lastname)_\(death.date.timeIntervalSinceReferenceDate)")
        userImageView.image = image
        
    }
    
}
